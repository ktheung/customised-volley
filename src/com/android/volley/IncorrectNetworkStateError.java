package com.android.volley;

@SuppressWarnings("serial")
public class IncorrectNetworkStateError extends NetworkError {
	 public IncorrectNetworkStateError() {
	        super();
	    }

	
	    public IncorrectNetworkStateError(Throwable reason) {
	        super(reason);
	    }
}
